-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema BibliotecaEscolar
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema BibliotecaEscolar
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `BibliotecaEscolar` DEFAULT CHARACTER SET utf8 ;
USE `BibliotecaEscolar` ;

-- -----------------------------------------------------
-- Table `BibliotecaEscolar`.`libros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BibliotecaEscolar`.`libros` (
  `idlibros` INT NULL,
  `nombre_libro` VARCHAR(45) NULL,
  `autor` VARCHAR(45) NULL,
  `editorial` VARCHAR(45) NULL,
  PRIMARY KEY (`idlibros`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BibliotecaEscolar`.`Personal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BibliotecaEscolar`.`Personal` (
  `idPersonal` VARCHAR(45) NULL,
  `nombre` VARCHAR(45) NULL,
  `apellido` VARCHAR(45) NULL,
  `correo` VARCHAR(45) NULL,
  PRIMARY KEY (`idPersonal`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BibliotecaEscolar`.`prestamo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BibliotecaEscolar`.`prestamo` (
  `idprestamo` INT NULL,
  `Personal_idPersonal` INT NULL,
  `libros_idlibros` INT NULL,
  `fecha_salida` DATE NULL,
  `fecha_a_entregar` DATE NULL,
  PRIMARY KEY (`idprestamo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BibliotecaEscolar`.`maestro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BibliotecaEscolar`.`maestro` (
  `Personal_idPersonal` VARCHAR(45) NULL,
  `especialidad` VARCHAR(50) NULL,
  `telefono` VARCHAR(45) NULL,
  `domicilio` VARCHAR(45) NULL,
  PRIMARY KEY (`Personal_idPersonal`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BibliotecaEscolar`.`Alumno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BibliotecaEscolar`.`Alumno` (
  `Personal_idPersonal` VARCHAR(45) NULL,
  `grupo` VARCHAR(45) NULL,
  `carrera` VARCHAR(45) NULL,
  `nss` VARCHAR(45) NULL,
  PRIMARY KEY (`Personal_idPersonal`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
