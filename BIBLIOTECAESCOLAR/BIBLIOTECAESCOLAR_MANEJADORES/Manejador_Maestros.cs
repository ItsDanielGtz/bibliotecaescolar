﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIBLIOTECAESCOLAR_ENTIDADES;
using System.Data;
using BIBLIOTECAESCOLAR_ACCESOADATOS;
using System.Windows.Forms;


namespace BIBLIOTECAESCOLAR_MANEJADORES
{
    public class Manejador_Maestros
    {
        Conexion_Biblioteca ac = new Conexion_Biblioteca();
        public string GuardarDatos(Entidades_Maestros maestros)
        {
            return ac.comando(string.Format("INSERT INTO Maestro VALUES(" +
           "'{0}','{1}','{2}','{3}')", maestros.Fk_idPersonal, maestros.Especialidad, maestros.Telefono, maestros.Domicilio));
        }
        public string ModificarDatos(Entidades_Maestros maestros)
        {
            return ac.comando(string.Format("update maestro set especialidad='{0}', " + "telefono='{1}', domicilio='{2}' where Personal_idPersonal='{3}'", maestros.Especialidad, maestros.Telefono, maestros.Domicilio, maestros.Fk_idPersonal));
        }
        public string EliminarDatos(Entidades_Maestros maestros)
        {

            return ac.comando(string.Format("delete from maestro where Personal_idPersona='{0}'", maestros.Fk_idPersonal));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ac.Mostrar(q, tabla);
        }
        public void LlenarMastros(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ac.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPersonal";
        }

    }
}
