﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIBLIOTECAESCOLAR_ENTIDADES;
using System.Data;
using BIBLIOTECAESCOLAR_ACCESOADATOS;
using System.Windows.Forms;



namespace BIBLIOTECAESCOLAR_MANEJADORES
{
    public class Manejador_Prestamo
    {
        
            Conexion_Biblioteca ac = new Conexion_Biblioteca();
        public string Guardar(Entidades_Prestamo prestamo)
        {
            return ac.comando(string.Format("insert into prestamo values(" +
                "'{0}','{1}','{2}','{3}','{4}')", prestamo.Idprestamo, prestamo.Fk_idpersonal, prestamo.Fk_libro, prestamo.Fecha_Salida, prestamo.Fecha_Entrega));
        }
        public string ModificarDatos(Entidades_Prestamo prestamo)
            {
                return ac.comando(string.Format("UPDATE prestamo SET fecha_salida ='{0}'," + "fecha_a_entregar = '{1}' where idprestamo = '{2}'", prestamo.Fecha_Salida, prestamo.Fecha_Entrega, prestamo.Idprestamo));
            }
            public string EliminarDatos(Entidades_Prestamo prestamo)
            {

            return ac.comando(string.Format("delete from prestamo where idprestamo='{0}'", prestamo.Idprestamo));
        }
            public DataSet Listado(string q, string tabla)
            {
                return ac.Mostrar(q, tabla);
            }
        public void LlenarIdPersonal(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ac.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPersonal";
        }
        public void LlenarLibros(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ac.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idlibros";
        }

    }
}

