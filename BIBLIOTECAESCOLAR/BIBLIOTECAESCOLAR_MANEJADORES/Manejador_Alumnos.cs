﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIBLIOTECAESCOLAR_ENTIDADES;
using System.Data;
using BIBLIOTECAESCOLAR_ACCESOADATOS;
using System.Windows.Forms;


namespace BIBLIOTECAESCOLAR_MANEJADORES
{
    public class Manejador_alumnos
    {
        Conexion_Biblioteca ac = new Conexion_Biblioteca();
        public string GuardarDatos(Entidades_Alumnos alumnos)
        {
            return ac.comando(string.Format("INSERT INTO Alumno VALUES(" +
           "'{0}','{1}','{2}','{3}')", alumnos.Fk_idpersonal, alumnos.Grupo, alumnos.Carrera, alumnos.NSS));
        }
        public string Modificar(Entidades_Alumnos alumnos)
        {
            return ac.comando(string.Format("update Alumno set grupo='{0}', " + "carrera='{1}', nss='{2}' where Personal_idPersonal='{3}'", alumnos.Grupo, alumnos.Carrera, alumnos.NSS, alumnos.Fk_idpersonal));
        }
        public string EliminarDatos(Entidades_Alumnos alumnos)
        {

            return ac.comando(string.Format("delete from Alumno where Personal_idPersona='{0}'", alumnos.Fk_idpersonal));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ac.Mostrar(q, tabla);
        }
        public void LlenarAlumnos(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ac.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPersonal";
        }

    }
}
