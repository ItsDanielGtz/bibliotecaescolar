﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIBLIOTECAESCOLAR_ENTIDADES;
using System.Data;
using BIBLIOTECAESCOLAR_ACCESOADATOS;


namespace BIBLIOTECAESCOLAR_MANEJADORES
{
    public class Manejador_Libro
    {
        //Entidades_Libros el = new Entidades_Libros();
        Conexion_Biblioteca ac = new Conexion_Biblioteca();
        
        public string GuardarDatos(Entidades_Libros libro)
        {
            
            return ac.comando(string.Format("INSERT INTO libros VALUES(" +
           "{0},'{1}','{2}','{3}')", libro.Idlibros, libro.Nombre_libro, libro.Autor, libro.Editorial));

        }
        public string Borrar(Entidades_Libros libro)
        {
            return ac.comando(string.Format("delete from libros where idlibros  ='{0}'", libro.Idlibros));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ac.Mostrar(q, tabla);
        }

    }
}
