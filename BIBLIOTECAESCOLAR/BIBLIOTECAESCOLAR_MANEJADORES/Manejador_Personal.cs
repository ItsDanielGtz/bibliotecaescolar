﻿using BIBLIOTECAESCOLAR_ENTIDADES;
using System.Data;
using BIBLIOTECAESCOLAR_ACCESOADATOS;


namespace BIBLIOTECAESCOLAR_MANEJADORES
{
    public class Manejador_Personal
    {
        Conexion_Biblioteca ac = new Conexion_Biblioteca();
        public string GuardarDatos(Entidades_Personal personal)
        {
            return ac.comando(string.Format("INSERT INTO Personal VALUES(" +
           "'{0}','{1}','{2}','{3}')", personal.Idpersonal, personal.Nombre, personal.Apellido, personal.Correo));
        }
        public string ModificarDatos(Entidades_Personal personal)
        {
            return ac.comando(string.Format("UPDATE Personal SET nombre ='{0}'," + "apellido = '{1}',correo = '{2}' where idPersonal = '{3}'", personal.Nombre, personal.Apellido, personal.Correo, personal.Idpersonal));
        }
        public string EliminarDatos(Entidades_Personal personal)
        {

            return ac.comando(string.Format("delete from personal where idPersonal='{0}'", personal.Idpersonal));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ac.Mostrar(q, tabla);
        }

    }
}
