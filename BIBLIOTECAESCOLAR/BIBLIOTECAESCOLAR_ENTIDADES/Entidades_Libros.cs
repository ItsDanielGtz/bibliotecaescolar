﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIBLIOTECAESCOLAR_ENTIDADES
{
    public class Entidades_Libros
    {
        public int Idlibros { get; set; }
        public string Nombre_libro { get; set; } 
        public string Autor { get; set; }
        public string Editorial { get; set; }
        public Entidades_Libros(int idlibros, string nombre_libro, string autor, string editorial)
        {
            Idlibros = idlibros;
            Nombre_libro = nombre_libro;
            Autor = autor;
            Editorial = editorial;
        }
    }
}
