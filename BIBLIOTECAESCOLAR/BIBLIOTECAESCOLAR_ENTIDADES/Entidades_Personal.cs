﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIBLIOTECAESCOLAR_ENTIDADES
{
    public class Entidades_Personal
    {
        public string Idpersonal { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Correo { get; set; }
        public Entidades_Personal(string idpersonal, string nombre, string apellido, string correo)
        {
            Idpersonal = idpersonal;
            Nombre = nombre;
            Apellido = apellido;
            Correo = correo;
        }
    }
}
