﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIBLIOTECAESCOLAR_ENTIDADES
{
    public class Entidades_Alumnos
    {
        public string Fk_idpersonal { get; set; }
        public string Grupo { get; set; }
        public string Carrera { get; set; }
        public string NSS { get; set; }

        public Entidades_Alumnos(string fk_idpersonal, string grupo, string carrera, string nss)
        {
            Fk_idpersonal = fk_idpersonal;
            Grupo = grupo;
            Carrera = carrera;
            NSS = nss;
        }
    }
}
