﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIBLIOTECAESCOLAR_ENTIDADES
{
    public class Entidades_Prestamo
    {
        public int Idprestamo { get; set; }
        public string Fk_idpersonal { get; set; }
        public int Fk_libro { get; set; }
        public string Fecha_Salida { get; set; }
        public string Fecha_Entrega { get; set; }
        public Entidades_Prestamo(int idprestamo, string fk_idpersonal,int  fk_libro, string fecha_salida, string fecha_entrega )
        {
            Idprestamo = idprestamo;
            Fk_idpersonal = fk_idpersonal;
            Fk_libro = fk_libro;
            Fecha_Salida = fecha_salida;
            Fecha_Entrega = fecha_entrega;
        }
    }
}
