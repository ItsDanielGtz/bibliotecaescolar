﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIBLIOTECAESCOLAR_ENTIDADES
{
    public class Entidades_Maestros
    {
        public string Fk_idPersonal { get; set; }
        public string Especialidad { get; set; }
        public string Telefono { get; set; }
        public string Domicilio { get; set; }
        public Entidades_Maestros(string fk_idpersonal, string especialodad, string telefono, string domicilio)
        {
            Fk_idPersonal = fk_idpersonal;
            Especialidad = especialodad;
            Telefono = telefono;
            Domicilio = domicilio;
        }
    }
}
