﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BIBLIOTECAESCOLAR_ENTIDADES;
using BIBLIOTECAESCOLAR_MANEJADORES;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    
    public partial class Frm_Alumnos : Form
    {
        int fila = 0;
        int Id = 0;
        string r = "";
        Manejador_alumnos ma;
        Entidades_Alumnos al = new Entidades_Alumnos("", "", "", "");
        public Frm_Alumnos()
        {
            InitializeComponent();
            ma = new Manejador_alumnos();
        }
        void actualizar()
        {
            DtgAlumno.DataSource = ma.Listado(string.Format("" +
                 "select * from Alumno"), "Alumno").Tables[0];
            DtgAlumno.AutoResizeColumns();
            DtgAlumno.Columns[0].ReadOnly = true;

        }

        private void Frm_Alumnos_Load(object sender, EventArgs e)
        {
            actualizar();
            ma.LlenarAlumnos(CmbIdPersonal, "select idPersonal from Personal", "Personal");
        }

        private void DtgAlumno_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            al.Fk_idpersonal = DtgAlumno.Rows[fila].Cells[0].Value.ToString();
            al.Grupo = DtgAlumno.Rows[fila].Cells[1].Value.ToString();
            al.Carrera = DtgAlumno.Rows[fila].Cells[2].Value.ToString();
            al.NSS = DtgAlumno.Rows[fila].Cells[3].Value.ToString();
            CmbIdPersonal.Text = al.Fk_idpersonal;
            TxtGrupo.Text = al.Grupo;
            TxtCarrera.Text = al.Carrera;
            TxtNSS.Text = al.NSS;
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                al = new Entidades_Alumnos(Id.ToString(), TxtGrupo.Text, TxtCarrera.Text, TxtNSS.Text);
                actualizar();


                //Close();
            }
            else
                try
                {
                    string rs = ma.GuardarDatos(new Entidades_Alumnos(IdAlumno.Text, TxtGrupo.Text, TxtCarrera.Text, TxtNSS.Text));
                    //Close();
                    MessageBox.Show("Se ha guardado correctamente");
                    actualizar();

                }
                catch (Exception)
                {
                    MessageBox.Show("Error");
                    TxtGrupo.Focus();
                }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new Entidades_Alumnos(al.Fk_idpersonal, TxtGrupo.Text,TxtCarrera.Text,TxtNSS.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
                
            }
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar al Alumno",
            "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = ma.EliminarDatos(al);
                actualizar();
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el la informacion del alumno",
           "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = ma.EliminarDatos(al);
                actualizar();
            }
        }
    }
}
