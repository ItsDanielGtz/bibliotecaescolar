﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    public partial class Frm_Menu : Form
    {
        public Frm_Menu()
        {
            InitializeComponent();
        }

        private void Frm_Menu_Load(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Frm_Libros c = new Frm_Libros();
            c.MdiParent = this;
            c.Show();
        }

        private void TSUsuario_Click(object sender, EventArgs e)
        {
            Frm_Personal c = new Frm_Personal();
            c.MdiParent = this;
            c.Show();
        }

        private void TSMaestro_Click(object sender, EventArgs e)
        {
            Frm_Maestros c = new Frm_Maestros();
            c.MdiParent = this;
            c.Show();
        }

        private void TSAlumno_Click(object sender, EventArgs e)
        {
            Frm_Alumnos c = new Frm_Alumnos();
            c.MdiParent = this;
            c.Show();
        }

        private void TSPrestamo_Click(object sender, EventArgs e)
        {
            Frm_Prestamo c = new Frm_Prestamo();
            c.MdiParent = this;
            c.Show();
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
