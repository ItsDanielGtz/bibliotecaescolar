﻿using System;
using System.Windows.Forms;
using BIBLIOTECAESCOLAR_ENTIDADES;
using BIBLIOTECAESCOLAR_MANEJADORES;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    public partial class Frm_Personal : Form
    {
        int fila = 0;
        string r = "";
        Manejador_Personal mp;
        Entidades_Personal per = new Entidades_Personal("", "", "","");
        public Frm_Personal()
        {
            InitializeComponent();
            mp = new Manejador_Personal();
        }
        void actualizar()
        {
            DtgPersonal.DataSource = mp.Listado(string.Format("" +
                 "select * from personal"), "personal").Tables[0];
            DtgPersonal.AutoResizeColumns();
            DtgPersonal.Columns[0].ReadOnly = true;

        }
        private void Frm_Personal_Load(object sender, EventArgs e)
        {
            actualizar();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
                try
                {
                    string rs = mp.GuardarDatos(new Entidades_Personal(TxtIdPersonal.Text,TxtNombre.Text,TxtApellido.Text,TxtCorreo.Text));
                    //Close();
                    MessageBox.Show("Se ha guardado correctamente");
                    actualizar();

                }
                catch (Exception)
                {
                    MessageBox.Show("Error");
                    
                }
        }

        private void DtgPersonal_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            per.Idpersonal = DtgPersonal.Rows[fila].Cells[0].Value.ToString();
            per.Nombre = DtgPersonal.Rows[fila].Cells[1].Value.ToString();
            per.Apellido = DtgPersonal.Rows[fila].Cells[2].Value.ToString();
            per.Correo = DtgPersonal.Rows[fila].Cells[3].Value.ToString();
            TxtIdPersonal.Text = per.Idpersonal;
            TxtNombre.Text = per.Nombre;
            TxtApellido.Text = per.Apellido;
            TxtCorreo.Text = per.Correo;
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {

            try
            {
                string rs = mp.ModificarDatos(new Entidades_Personal(per.Idpersonal, TxtNombre.Text, TxtApellido.Text, TxtCorreo.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar al usuario",
            "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mp.EliminarDatos(per);
                actualizar();
            }
        }

        private void DtgPersonal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
