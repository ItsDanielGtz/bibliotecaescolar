﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BIBLIOTECAESCOLAR_ENTIDADES;
using BIBLIOTECAESCOLAR_MANEJADORES;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    public partial class Frm_Maestros : Form
    {
        int fila = 0;
        int Id = 0;
        string r = "";
        Manejador_Maestros mm;
        Entidades_Maestros mae = new Entidades_Maestros("", "", "", "");
        public Frm_Maestros()
        {
            InitializeComponent();
            mm = new Manejador_Maestros();
        }
        void actualizar()
        {
            DtgMaestro.DataSource = mm.Listado(string.Format("" +
                 "select * from maestro"), "maestro").Tables[0];
            DtgMaestro.AutoResizeColumns();
            DtgMaestro.Columns[0].ReadOnly = true;
        }

        private void Frm_Maestros_Load(object sender, EventArgs e)
        {
            actualizar();
            mm.LlenarMastros(CmbIdPersonal, "select idPersonal from Personal", "Personal");
        }

        private void DtgMaestro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            mae.Fk_idPersonal = DtgMaestro.Rows[fila].Cells[0].Value.ToString();
            mae.Especialidad = DtgMaestro.Rows[fila].Cells[1].Value.ToString();
            mae.Telefono = DtgMaestro.Rows[fila].Cells[2].Value.ToString();
            mae.Domicilio = DtgMaestro.Rows[fila].Cells[3].Value.ToString();
            CmbIdPersonal.Text = mae.Fk_idPersonal;
            TxtEspecialidad.Text = mae.Especialidad;
            TxtTelefono.Text = mae.Telefono;
            TxtDomicilio.Text = mae.Domicilio;
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                mae = new Entidades_Maestros(Id.ToString(), TxtEspecialidad.Text, TxtTelefono.Text, TxtDomicilio.Text);
                actualizar();


                //Close();
            }
            else
                try
                {
                    string rs = mm.GuardarDatos(new Entidades_Maestros(CmbIdPersonal.Text, TxtEspecialidad.Text, TxtTelefono.Text, TxtDomicilio.Text));
                    //Close();
                    MessageBox.Show("Se ha guardado correctamente");
                    actualizar();

                }
                catch (Exception)
                {
                    MessageBox.Show("Error");

                }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mm.ModificarDatos(new Entidades_Maestros(mae.Fk_idPersonal, TxtEspecialidad.Text, TxtTelefono.Text, TxtDomicilio.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar la informacion del maestro",
            "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mm.EliminarDatos(mae);
                actualizar();
            }
        }

        private void Frm_Maestros_Leave(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
