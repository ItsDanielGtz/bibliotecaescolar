﻿namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    partial class Frm_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Menu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TSLibro = new System.Windows.Forms.ToolStripButton();
            this.TSUsuario = new System.Windows.Forms.ToolStripButton();
            this.TSMaestro = new System.Windows.Forms.ToolStripButton();
            this.TSAlumno = new System.Windows.Forms.ToolStripButton();
            this.TSPrestamo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSLibro,
            this.TSUsuario,
            this.TSMaestro,
            this.TSAlumno,
            this.TSPrestamo,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(69, 461);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // TSLibro
            // 
            this.TSLibro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSLibro.Image = ((System.Drawing.Image)(resources.GetObject("TSLibro.Image")));
            this.TSLibro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSLibro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSLibro.Name = "TSLibro";
            this.TSLibro.Size = new System.Drawing.Size(66, 68);
            this.TSLibro.Text = " + Libros";
            this.TSLibro.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TSUsuario
            // 
            this.TSUsuario.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSUsuario.Image = ((System.Drawing.Image)(resources.GetObject("TSUsuario.Image")));
            this.TSUsuario.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSUsuario.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSUsuario.Name = "TSUsuario";
            this.TSUsuario.Size = new System.Drawing.Size(66, 68);
            this.TSUsuario.Text = "+ Usuario";
            this.TSUsuario.Click += new System.EventHandler(this.TSUsuario_Click);
            // 
            // TSMaestro
            // 
            this.TSMaestro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSMaestro.Image = ((System.Drawing.Image)(resources.GetObject("TSMaestro.Image")));
            this.TSMaestro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSMaestro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSMaestro.Name = "TSMaestro";
            this.TSMaestro.Size = new System.Drawing.Size(66, 68);
            this.TSMaestro.Text = "+ Maestro";
            this.TSMaestro.Click += new System.EventHandler(this.TSMaestro_Click);
            // 
            // TSAlumno
            // 
            this.TSAlumno.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSAlumno.Image = ((System.Drawing.Image)(resources.GetObject("TSAlumno.Image")));
            this.TSAlumno.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSAlumno.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSAlumno.Name = "TSAlumno";
            this.TSAlumno.Size = new System.Drawing.Size(66, 68);
            this.TSAlumno.Text = "+ Alumno";
            this.TSAlumno.Click += new System.EventHandler(this.TSAlumno_Click);
            // 
            // TSPrestamo
            // 
            this.TSPrestamo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSPrestamo.Image = ((System.Drawing.Image)(resources.GetObject("TSPrestamo.Image")));
            this.TSPrestamo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TSPrestamo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSPrestamo.Name = "TSPrestamo";
            this.TSPrestamo.Size = new System.Drawing.Size(66, 68);
            this.TSPrestamo.Text = "Prestamo";
            this.TSPrestamo.Click += new System.EventHandler(this.TSPrestamo_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(66, 68);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // Frm_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 461);
            this.Controls.Add(this.toolStrip1);
            this.IsMdiContainer = true;
            this.Name = "Frm_Menu";
            this.Text = "PAGINA PRINCIPAL";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Menu_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton TSLibro;
        private System.Windows.Forms.ToolStripButton TSUsuario;
        private System.Windows.Forms.ToolStripButton TSMaestro;
        private System.Windows.Forms.ToolStripButton TSAlumno;
        private System.Windows.Forms.ToolStripButton TSPrestamo;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}