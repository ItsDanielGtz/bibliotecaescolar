﻿namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    partial class Frm_Prestamo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnEliminar = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DtgPrestamo = new System.Windows.Forms.DataGridView();
            this.IdPersonal = new System.Windows.Forms.Label();
            this.TxtIdprestamo = new System.Windows.Forms.TextBox();
            this.BtnAgregar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.DTPFechaSalida = new System.Windows.Forms.DateTimePicker();
            this.DTPFechaEntregar = new System.Windows.Forms.DateTimePicker();
            this.CmbIdLibro = new System.Windows.Forms.ComboBox();
            this.CmbIdPersonal = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DtgPrestamo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(125)))), ((int)(((byte)(100)))));
            this.BtnEliminar.FlatAppearance.BorderSize = 0;
            this.BtnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEliminar.Location = new System.Drawing.Point(101, 296);
            this.BtnEliminar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.Size = new System.Drawing.Size(87, 28);
            this.BtnEliminar.TabIndex = 23;
            this.BtnEliminar.Text = "Eliminar";
            this.BtnEliminar.UseVisualStyleBackColor = false;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(125)))), ((int)(((byte)(100)))));
            this.BtnModificar.FlatAppearance.BorderSize = 0;
            this.BtnModificar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnModificar.Location = new System.Drawing.Point(166, 255);
            this.BtnModificar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(87, 28);
            this.BtnModificar.TabIndex = 22;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseVisualStyleBackColor = false;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.label3.Location = new System.Drawing.Point(28, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "Fecha Salida";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.label2.Location = new System.Drawing.Point(59, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "IdLibro";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.label1.Location = new System.Drawing.Point(38, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "IdPersonal";
            // 
            // DtgPrestamo
            // 
            this.DtgPrestamo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgPrestamo.Location = new System.Drawing.Point(293, 44);
            this.DtgPrestamo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtgPrestamo.Name = "DtgPrestamo";
            this.DtgPrestamo.Size = new System.Drawing.Size(596, 194);
            this.DtgPrestamo.TabIndex = 15;
            this.DtgPrestamo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgPrestamo_CellClick);
            // 
            // IdPersonal
            // 
            this.IdPersonal.AutoSize = true;
            this.IdPersonal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.IdPersonal.Location = new System.Drawing.Point(38, 48);
            this.IdPersonal.Name = "IdPersonal";
            this.IdPersonal.Size = new System.Drawing.Size(69, 16);
            this.IdPersonal.TabIndex = 14;
            this.IdPersonal.Text = "IdPrestamo";
            // 
            // TxtIdprestamo
            // 
            this.TxtIdprestamo.Location = new System.Drawing.Point(115, 44);
            this.TxtIdprestamo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TxtIdprestamo.Name = "TxtIdprestamo";
            this.TxtIdprestamo.ReadOnly = true;
            this.TxtIdprestamo.Size = new System.Drawing.Size(154, 21);
            this.TxtIdprestamo.TabIndex = 13;
            this.TxtIdprestamo.TextChanged += new System.EventHandler(this.TxtIdprestamo_TextChanged);
            // 
            // BtnAgregar
            // 
            this.BtnAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(125)))), ((int)(((byte)(100)))));
            this.BtnAgregar.FlatAppearance.BorderSize = 0;
            this.BtnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAgregar.Location = new System.Drawing.Point(41, 255);
            this.BtnAgregar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnAgregar.Name = "BtnAgregar";
            this.BtnAgregar.Size = new System.Drawing.Size(87, 28);
            this.BtnAgregar.TabIndex = 12;
            this.BtnAgregar.Text = "Agregar";
            this.BtnAgregar.UseVisualStyleBackColor = false;
            this.BtnAgregar.Click += new System.EventHandler(this.BtnAgregar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.label4.Location = new System.Drawing.Point(6, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "Fecha a entregar";
            // 
            // DTPFechaSalida
            // 
            this.DTPFechaSalida.Location = new System.Drawing.Point(115, 181);
            this.DTPFechaSalida.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DTPFechaSalida.Name = "DTPFechaSalida";
            this.DTPFechaSalida.Size = new System.Drawing.Size(154, 21);
            this.DTPFechaSalida.TabIndex = 26;
            // 
            // DTPFechaEntregar
            // 
            this.DTPFechaEntregar.Location = new System.Drawing.Point(115, 214);
            this.DTPFechaEntregar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DTPFechaEntregar.Name = "DTPFechaEntregar";
            this.DTPFechaEntregar.Size = new System.Drawing.Size(154, 21);
            this.DTPFechaEntregar.TabIndex = 27;
            // 
            // CmbIdLibro
            // 
            this.CmbIdLibro.FormattingEnabled = true;
            this.CmbIdLibro.Location = new System.Drawing.Point(115, 130);
            this.CmbIdLibro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CmbIdLibro.Name = "CmbIdLibro";
            this.CmbIdLibro.Size = new System.Drawing.Size(154, 24);
            this.CmbIdLibro.TabIndex = 28;
            // 
            // CmbIdPersonal
            // 
            this.CmbIdPersonal.FormattingEnabled = true;
            this.CmbIdPersonal.Location = new System.Drawing.Point(115, 85);
            this.CmbIdPersonal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CmbIdPersonal.Name = "CmbIdPersonal";
            this.CmbIdPersonal.Size = new System.Drawing.Size(154, 24);
            this.CmbIdPersonal.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.label5.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.LightGray;
            this.label5.Location = new System.Drawing.Point(394, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(291, 33);
            this.label5.TabIndex = 32;
            this.label5.Text = "AGREGAR PRESTAMO";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(140)))), ((int)(((byte)(88)))));
            this.panel1.Controls.Add(this.BtnModificar);
            this.panel1.Controls.Add(this.BtnEliminar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 337);
            this.panel1.TabIndex = 33;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(125)))), ((int)(((byte)(100)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(765, 255);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 28);
            this.button1.TabIndex = 24;
            this.button1.Text = "CERRAR VENTANA";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Frm_Prestamo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.ClientSize = new System.Drawing.Size(918, 337);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CmbIdPersonal);
            this.Controls.Add(this.CmbIdLibro);
            this.Controls.Add(this.DTPFechaEntregar);
            this.Controls.Add(this.DTPFechaSalida);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DtgPrestamo);
            this.Controls.Add(this.IdPersonal);
            this.Controls.Add(this.TxtIdprestamo);
            this.Controls.Add(this.BtnAgregar);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Frm_Prestamo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Prestamo";
            this.Load += new System.EventHandler(this.Frm_Prestamo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgPrestamo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnEliminar;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DtgPrestamo;
        private System.Windows.Forms.Label IdPersonal;
        private System.Windows.Forms.TextBox TxtIdprestamo;
        private System.Windows.Forms.Button BtnAgregar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DTPFechaSalida;
        private System.Windows.Forms.DateTimePicker DTPFechaEntregar;
        private System.Windows.Forms.ComboBox CmbIdLibro;
        private System.Windows.Forms.ComboBox CmbIdPersonal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
    }
}