﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BIBLIOTECAESCOLAR_ENTIDADES;
using BIBLIOTECAESCOLAR_MANEJADORES;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
    public partial class Frm_Libros : Form
    {
        int fila = 0;
        string r = "";
        Manejador_Libro Mi;
        Entidades_Libros li = new Entidades_Libros(0, "", "", "");
        public Frm_Libros()
        {
            InitializeComponent();
            Mi = new Manejador_Libro();
        }
        void actualizar()
        {
            DtgLibro.DataSource = Mi.Listado(string.Format("" +
                 "select * from libros"), "libros").Tables[0];
            DtgLibro.AutoResizeColumns();
            DtgLibro.Columns[0].ReadOnly = true;

        }
        private void Frm_Libros_Load(object sender, EventArgs e)
        {
            actualizar();
        }

        private void DtgLibro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            li.Idlibros = int.Parse(DtgLibro.Rows[fila].Cells[0].Value.ToString());
            li.Nombre_libro = DtgLibro.Rows[fila].Cells[1].Value.ToString();
            li.Autor = DtgLibro.Rows[fila].Cells[2].Value.ToString();
            li.Editorial = DtgLibro.Rows[fila].Cells[3].Value.ToString();
            TxtIdLibro.Text = li.Idlibros.ToString();
            TxtNomLibro.Text = li.Nombre_libro;
            TxtAutor.Text = li.Autor;
            TxtEditorial.Text = li.Editorial;

        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
                try
            {
                string rs = Mi.GuardarDatos(new Entidades_Libros(0, TxtNomLibro.Text, TxtAutor.Text, TxtEditorial.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el libro",
            "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = Mi.Borrar(li);
                actualizar();
            }
        }

        private void IdLibros_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
