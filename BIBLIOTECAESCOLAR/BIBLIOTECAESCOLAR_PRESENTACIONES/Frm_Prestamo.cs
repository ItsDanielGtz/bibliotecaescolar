﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BIBLIOTECAESCOLAR_ENTIDADES;
using BIBLIOTECAESCOLAR_MANEJADORES;

namespace BIBLIOTECAESCOLAR_PRESENTACIONES
{
   
    public partial class Frm_Prestamo : Form
    {
        int fila = 0;
        int Id = 0;
        string r = "";
        Manejador_Prestamo mp;
        Entidades_Prestamo pre = new Entidades_Prestamo(0, "", 0, "", "");
        public Frm_Prestamo()
        {
            InitializeComponent();
            mp = new Manejador_Prestamo();
        }
        void actualizar()
        {
            DtgPrestamo.DataSource = mp.Listado(string.Format("" +
                 "select * from prestamo"), "prestamo").Tables[0];
            DtgPrestamo.AutoResizeColumns();
            DtgPrestamo.Columns[0].ReadOnly = true;

        }
        private void Frm_Prestamo_Load(object sender, EventArgs e)
        {
            actualizar();
            
            mp.LlenarIdPersonal(CmbIdPersonal, "select idPersonal from Personal", "Personal");
            mp.LlenarLibros(CmbIdLibro, "select idlibros from libros", "libros");

            DTPFechaEntregar.Format = DateTimePickerFormat.Custom;
            DTPFechaEntregar.CustomFormat = "yyyy-MM-dd";
            DTPFechaSalida.Format = DateTimePickerFormat.Custom;
            DTPFechaSalida.CustomFormat = "yyyy-MM-dd";
        }

        private void DtgPrestamo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            pre.Idprestamo = int.Parse(DtgPrestamo.Rows[fila].Cells[0].Value.ToString());
            pre.Fk_idpersonal = DtgPrestamo.Rows[fila].Cells[1].Value.ToString();
            pre.Fk_libro = int.Parse(DtgPrestamo.Rows[fila].Cells[2].Value.ToString());
            pre.Fecha_Salida = DtgPrestamo.Rows[fila].Cells[3].Value.ToString();
            pre.Fecha_Entrega = DtgPrestamo.Rows[fila].Cells[3].Value.ToString();
            TxtIdprestamo.Text = pre.Idprestamo.ToString();
            CmbIdPersonal.Text = pre.Fk_idpersonal;
            CmbIdLibro.Text = pre.Fk_libro.ToString();
            DTPFechaEntregar.Text = pre.Fecha_Salida;
            DTPFechaSalida.Text = pre.Fecha_Entrega;
           
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            
                try
                {
                    string rs = mp.Guardar(new Entidades_Prestamo(0, CmbIdPersonal.Text, int.Parse(CmbIdLibro.Text), DTPFechaSalida.Text, DTPFechaEntregar.Text));
                    //Close();
                    MessageBox.Show("Se ha guardado correctamente");
                    actualizar();

                }
                catch (Exception)
                {
                    MessageBox.Show("Error");
                    
                }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mp.ModificarDatos(new Entidades_Prestamo(pre.Idprestamo,"",int.Parse(CmbIdLibro.Text),DTPFechaSalida.Text,DTPFechaEntregar.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el prestamo",
           "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mp.EliminarDatos(pre);
                actualizar();
            }
        }

        private void TxtIdprestamo_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();

        }
    }
}
